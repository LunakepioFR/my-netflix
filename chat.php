<?php
session_start();
require('connexion.php');

$t = time();

if(!isset($_SESSION['auth']))
{
header("Location: identification.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$idm = $_GET['idm'];
$ida = $_GET['ida'];


$query = 'SELECT * FROM membres WHERE id =?';
$req = $bdd->prepare($query);
$req->execute(array($ida));

$donneesami = $req->fetch();

?>


<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr" >
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="style.css?<?php echo $t ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
  <style type="text/css">

  </style>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
<li><a href="amis.php">Mes Amis</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>

<div style="color:white;"class="chat-wrapper">
<div class="chat">
    <div style="color:white;"class="chatheader">
    <img style="border-radius:50%; width:25px;vertical-align:middle;"src="<?php echo $donneesami['pfpic']?>"> &ensp;&ensp;
    <?php echo $donneesami['pseudo']?>


    </div>
    <div class="chattext">
   
    <div class="chatdroite">
    <?php
    $query = 'SELECT * FROM chat WHERE id_membre1=? AND id_membre2=?';
    $req = $bdd->prepare($query);
    $req->execute(array($idm, $ida));

    while($donneeschat1 = $req->fetch()){
      ?>
      <div class="chatenvoyer">
      <?php echo $donneeschat1['message']?>
      
      </div>
 
      <?php
    }
    ?>
     <div class="chatgauche">
      <?php
    
        $query2 = 'SELECT * FROM chat WHERE id_membre1=? AND id_membre2=?';
        $req2 = $bdd->prepare($query2);
        $req2->execute(array($ida,$idm));

        while($donneeschat2 = $req2->fetch()){
      ?>
      <div class="chatrecu">
      <?php echo $donneeschat2['message']?>
      
      </div>
     
      <?php
    }
    ?>
     </div>
      

    </div>
    <div class="chatinput">
    <form action ="chatsend.php">
    <input type="text" name="message">
    <button type="submit">Envoyer</button>
    
    </form>
    
    </div>
</div>
</div>

<script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>

  </body>
</html>
