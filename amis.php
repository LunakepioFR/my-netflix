<?php
session_start();
require('connexion.php');

$t = time();

if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$id_membre = $_SESSION['id'];

$query = 'SELECT * FROM amis WHERE id_membres =?';
$req = $bdd->prepare($query);
$req->execute(array($id_membre));


$query2 = 'SELECT count(*) as nbr FROM amis WHERE id_membres =? and ami_valide =1';
$req2 = $bdd->prepare($query2);
$req2->execute(array($id_membre));

$nbramis = 0;

?>


<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr" style="color: white;" >
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="style.css?<?php echo $t ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
<li><a href="amis.php">Mes Amis</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>

<?php
while($nbramis = $req2->fetch())
{?>
<p style="margin-left:25px;" class="bonjour">Vous avez <?php echo $nbramis['nbr']?> ami(s).</p>
<?php
}
$query = 'SELECT * FROM amis WHERE id_membres =? AND ami_valide =1';
$req = $bdd->prepare($query);
$req->execute(array($id_membre));
while($amis = $req->fetch())
{
    $id_ami = $amis['id_membres_ami'];
    $query3 = 'SELECT * FROM membres WHERE id =?';
    $req3 = $bdd->prepare($query3);
    $req3->execute(array($id_ami));
    while($vraiamis = $req3->fetch())
    {
      ?>
      <div class="friends">
      <img style="border-radius:50%; width:25px;vertical-align:middle;" src="<?php echo $vraiamis['pfpic']?>">
      <?php echo $vraiamis['pseudo'];?>
      &ensp;
      <a href="chat.php?idm=<?php echo $id_membre?>&ida=<?php echo $id_ami?>"><img style="width:25px;vertical-align:middle;" src="chat.png"></a>
      &ensp;
      <a href="friendreview.php?ida=<?php echo $id_ami?>"><img style="width:25px;vertical-align:middle;" src="reviews.png"></a>
      &ensp;&ensp;&ensp;&ensp;
      <a href="deletefriend.php?idm=<?php echo $id_membre?>&ida=<?php echo $id_ami?>"><img style="width:25px;vertical-align:middle;" src="supprimer.png"></a>
        
      </div>
      <?php
    }
}
?>

<p style="margin-left:25px;margin-top:35px;" >Vous avez des invitations d'amis à répondre</p>

<?php

$query5 = 'SELECT * FROM amis WHERE id_membres =? AND ami_valide =0';
$req5 = $bdd->prepare($query5);
$req5->execute(array($id_membre));
while($invit = $req5->fetch())
{
    $id_amifaux = $invit['id_membres_ami'];
    $query4 = 'SELECT * FROM membres WHERE id =?';
    $req4 = $bdd->prepare($query4);
    $req4->execute(array($id_amifaux));
    while($fauxamis = $req4->fetch())
    {
      ?>
      <div class="friends">
      <img style="border-radius:50%; width:25px;vertical-align:middle;" src="<?php echo $fauxamis['pfpic']?>">
      <?php echo $fauxamis['pseudo'];?>
      &ensp;
      <a href="validfriend.php?idm=<?php echo $id_membre?>&ida=<?php echo $id_ami?>"><img style="width:25px;vertical-align:middle;" src="valid.png"></a>
      &ensp;&ensp;&ensp;&ensp;
      <a href="deletefriend.php?idm=<?php echo $id_membre?>&ida=<?php echo $id_ami?>"><img style="width:25px;vertical-align:middle;" src="supprimer.png"></a>
        
      </div>
      <?php
    }
}
?>
<script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
</script>

</body>
</html>