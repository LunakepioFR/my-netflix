<?php
session_start();
require('connexion.php');

$t = time();

if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$reponse = $bdd->query('SELECT DISTINCT series.nom as nom, series.id as id, series.image as image, series.resume as resume, avis.id_serie, avis.note FROM series, avis WHERE avis.id_serie = series.id AND avis.note >= 3 
ORDER BY `avis`.`note`  DESC LIMIT 1');
$top1=$reponse->fetch();
?>


<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="test.css?<?php echo $t ?>">
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
  <style type="text/css">


  .top {
    background-image:
    linear-gradient(to top, rgba(0,0,0, 1), rgba(0, 0, 0, 0) 20%),
    linear-gradient(to right, rgba(0,0,0, 1), rgba(0, 0, 0, 0) 55%),
    linear-gradient(to bottom, rgba(0,0,0, 1), rgba(0, 0, 0, 0) 10%),
    url('hunterbackground.jpg');
    background-position: right;
    background-repeat: no-repeat;
    width: 100%;
    height:650px;
    background-size: cover;
    color: white;
    padding: 20px;
}

  </style>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
    <section>
    <div class="top">
    <div class="bonjour">
        <p>Bonjour <?php echo $_SESSION['pseudo']?>, qu'allez-vous regarder aujourd'hui ?</p> 
    </div></br></br>
  <h3><img width="18.5" height="32.88px" src="logomyn.png"> Series</h3>
    <h1><span class="nomserie"><?php echo $top1['nom']?></span></h1>
    <h6><span class="number">Numéro 1 des utilisateurs </span></h6>
      <div class="gauche">
      <p><?php echo $top1['resume']?></p>
      </div>
      <div class="droite">
      </div>
    </div>
    </section>
    <section>
  <div class="horizontal-scrolling">
  <h2><span class="new">NOUVEAUX !</span></h2>
        <div class="dock">       


        <?php
        $reponse = $bdd->query('SELECT * FROM series ORDER BY date_creation ASC LIMIT 25');

// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
        ?>
            <a href="film.php?id=<?php echo $donnees['id']?>"><img width="173.485px" height="245.8625px" src="<?php echo $donnees['image']?>" alt="Series"/></a>

        <?php
        }

        $reponse->closeCursor(); // Termine le traitement de la requête

        ?>
      </div>
  </div>
    </section>
    </br></br>

    <section>
  <div class="horizontal-scrolling">
  <h2>Les 10 séries les mieux notés :</h2>
        <div class="dock">
        <?php
        $reponse = $bdd->query('SELECT DISTINCT series.id as id, series.image as image, avis.id_serie, avis.note FROM series, avis WHERE avis.id_serie = series.id AND avis.note >= 3 
        ORDER BY `avis`.`note`  DESC LIMIT 10');

// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
        ?>
            <a href="film.php?id=<?php echo $donnees['id']?>"><img width="173.485px" height="245.8625px" src="<?php echo $donnees['image']?>" alt="Series"/></a>

        <?php
        }

        $reponse->closeCursor(); // Termine le traitement de la requête

        ?>
      </div>
  </div>
    </section>
    </br></br>

    <section>
  <div class="horizontal-scrolling">
  <h2>Liste des séries et films :</h2>
        <div class="dock">       


        <?php
        $reponse = $bdd->query('SELECT * FROM series');

// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
        ?>
            <a href="film.php?id=<?php echo $donnees['id']?>"><img width="173.485px" height="245.8625px" src="<?php echo $donnees['image']?>" alt="Series"/></a>

        <?php
        }

        $reponse->closeCursor(); // Termine le traitement de la requête

        ?>
      </div>
  </div>
    </section>
      </br></br>
<script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>

  </body>
</html>
