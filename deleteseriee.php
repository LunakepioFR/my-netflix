<?php
session_start();
require('connexion.php');

if(!isset($_SESSION['auth']))
{
    header("Location: untest.php");
}

$id = $_GET['id'];
$reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
$film = $reponse->fetch();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />

    <link rel="stylesheet" type="text/css" href="style.css">

    <title>Modification <?php echo $film['nom']?></title>
</head>
<body>
  <header>
    <img class="logo" src="logo.png" alt="logo du site"/>
    <form method="GET" action="recherche.php">
    <input class="recherche" name="recherche" type="text" placeholder="Rechercher..">
    </form>
    <nav>
        <ul class="lien_nav">
            <li class="items"><a href="index.php">Accueil</a></li>
            <li class="items"><a href="#">Catégories</a></li>
            <li class="items"><a href="#">FAQ</a></li>
            <li class="toggle"><a href="#"><span class="bars"></span></a></li>
        </ul>
    </nav>
    <a class="contacter" href="logout.php"><button>Déconnexion</button></a>   
    <?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <a class="contacter" href="administration.php"><button>Administration</button></a>
            <?php
        }
        ?>
  </header>
  <section>

  </section>
    <div class="bonjour">
    <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </div>
    <div class="series">
        <div class="section">
        <form method="POST" action="deleteserie.php?id=<?php echo $id ?>"><p>
        <div class="section">
            <img width="314px" height="445px" src="<?php echo $film['image']?>"/>
        </div>
            Nom : <input type="text" name="nom" value="<?php echo $film['nom'] ?>">
       
        
            Thèmatique : <input type="text" name="thematique" value="<?php echo $film['thematique']?>">

            Résumer : <input type="text" name="resume" size="250" value="<?php echo $film['resume'] ?>"></p>

            lien trailer : <input type="text" name="trailer" value="<?php echo $film['trailer'] ?>">
            </p>
            <button class=contacter type=submit>Valider</button>
        </form>
</body>
</html>
