<?php
session_start();
require('connexion.php');




$t = time();

$id=1;
$reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
$film = $reponse->fetch();

$notation = $bdd->query("SELECT AVG(note) as moyenne FROM avis WHERE id_serie='{$id}'");
$note = $notation->fetch();
$moyenne = $note['moyenne'];
$moyenne = round($moyenne);
?>
<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="style.css?<?php echo $t ?>">
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
<div class="banner">
    <div class="conteneur">
    <h3><img width="23.75px" src="logomyn.png"> Series</h3>
        <h1><span class="nomserie"><?php echo $film['nom']?></span></h1>
        <p><?php echo $film['resume']?> </p>
        <a href="video.php?video=<?php echo $id?>" class="play" target="_blank"><img src="play.png">Regardez le trailer</a>
        <h2>Moyenne des utilisateurs :</br>
        <?php
        $i = 5;

        while($i != 0)
        {
          while($moyenne != 0)
          {
            ?><img src="Redstar2.png" width="50px" class="shadowfilter">
            <?php
            $moyenne = $moyenne - 1;
            $i = $i -1;
          }
          if ($i <= 0){
          break;
          }
        ?><img src="Greystar2.png" width="50px">
        <?php 
        $i = $i -1;
        }
?>
    </div>
</div>
<div id="compost">
            <section>
            <form method="POST" action="com.php?id=<?php echo $id?>">
            <div class="ecrirecom">
                <input class="formulaire" type="text" name="commentaire" placeholder="Ecrivez un commentaire .." required></br>
            </div>
            <div class="noter">
                <input class="formulaire" type="number" name="note" min="0" max="5" placeholder="/5" required/>&ensp;&ensp;
                <div class="uneboite">
                <button type="submit"><img src="send.png" width="45px"></button>
                </div>
                </div>
            </form>
        </section>
        <div class="commentaires">
        <?php
            $reponse = $bdd->query("SELECT avis.id_membre, avis.note as note, avis.commentaire as commentaire, avis.id_serie, avis.date_creation, membres.pseudo, preferences.id_membre, preferences.couleur_avis as couleur FROM avis, membres, preferences WHERE membres.id = avis.id_membre AND membres.id = preferences.id_membre AND avis.id_serie = '{$id}' ORDER BY date_creation ASC");
            // On affiche chaque entrée une à une
            $x = rand(1, 5);
            while ($donnees = $reponse->fetch())
            {
              $x = rand(1, 5);
              $note = $donnees['note'];
            ?>
                <div class="com" style="border: 3px solid <?php echo $donnees['couleur']?>;">
                <div style="" class="picture"><img width="35px" src="boo<?php echo $x?>.png"></div>
                <div class="actualcom">
                <h4 style="color:<?php echo $donnees['couleur']?>"><?php echo $donnees['pseudo'] ?> : </h4>
                <p><?php echo $donnees['commentaire']?></p>
                </div>
                <div class="lanote">
                <?php
        $i = 5;
        while($i != 0)
        {
          while($note != 0)
          {
            ?><img width="25px" src="Redstar2.png" width="50px" class="shadowfilter">
            <?php
            $note = $note- 1;
            $i = $i -1;
          }
          if ($i <= 0){
          break;
          }?>
        <img width="25px" src="Greystar2.png" width="50px">
        <?php 
        $i = $i -1;
        }
?>
</div>
                </div>
                </br>
                <?php
           }

           $reponse->closeCursor(); // Termine le traitement de la requête
   
           ?>
          </div>

         
   
        <script>

      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });



    </script>
</html>
