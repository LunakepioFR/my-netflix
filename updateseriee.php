<?php
session_start();
require('connexion.php');

if(!isset($_SESSION['auth']))
{
    header("Location: untest.php");
}

$id = $_GET['id'];
$reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
$film = $reponse->fetch();

?>
<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr" >
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="style.css?<?php echo $t ?>">
    <link rel="stylesheet" href="stylead.css?<?php echo $t ?>">
   <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


  </head>
  <body>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
  <section>

  </section>
    <div class="bonjour">
    <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </div>
    <div class="series">
        <div class="section">
        <form method="POST" action="updateserie.php?id=<?php echo $id ?>"><p>
        <div class="section">
            <img width="314px" height="445px" src="<?php echo $film['image']?>"/>
        </div>
        <div class="administrer">
            Nom : <input class="formulaire" type="text" name="nom" value="<?php echo $film['nom'] ?>"></br>
       
        
            Thèmatique : <input class="formulaire" type="text" name="thematique" value="<?php echo $film['thematique']?>"></br>

            Résumer : <input class="formulaire" type="text" name="resume" size="250" value="<?php echo $film['resume'] ?>"></p></br>

            lien trailer : <input class="formulaire" type="text" name="trailer" value="<?php echo $film['trailer'] ?>">
            </p>
            <button class=contacter type=submit>Valider</button>
            </div>
        </form>
        <script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>
</body>
</html>
