<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$t = time();
?>
<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr" >
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="style.css?<?php echo $t ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="stylesheet" href="stylerech.css?<?php echo $t ?>">
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
  <style type="text/css">

  </style>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
<li><a href="amis.php">Mes Amis</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
<div class="bonjour">
        <p>Bonjour <?php echo $_SESSION['pseudo']?> </p> </br></br></br>
    </div>
<div class="ligne">
        <div class="film">
<?php
$recherche=$_GET['recherche'];

    $reponse = $bdd->query('SELECT * FROM series WHERE nom LIKE "%'.$recherche.'%"');


$query2 = $bdd->query('SELECT count(*) as nbr FROM series WHERE nom LIKE "%'.$recherche.'%"');


while($nbrresult = $query2->fetch())
{?>
<p class="bonjour">Vous avez <?php echo $nbrresult['nbr']?> résultats dans votre requête.</p>
<?php
}
?>
<?php
    while ($donnees = $reponse->fetch())
        {
        ?>
            <a href="series.php?id=<?php echo $donnees['id']?>"><img style="margin-right:25px;"  src="<?php echo $donnees['image']?>" alt="Series"/></a>
        <?php
        }

        $reponse->closeCursor(); // Termine le traitement de la requête

        ?>
        </div>
</div>

<script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>
</section>
</html>
    