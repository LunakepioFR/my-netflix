<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Administration MyNetflix</title>
</head>
<body>
<header>
        <img class="logo" src="logo.png" alt="logo du site"/>
		<form method="GET" action="recherche.php">
        <input class="recherche" name="recherche" type="text" placeholder="Rechercher..">
		</form>
        <nav>
            <ul class="lien_nav">
                <li class="items"><a href="index.php">Accueil</a></li>
                <li class="deroulant"><a href="#">Catégories</a></li>
                <li class="items"><a href="#">FAQ</a></li>
                <li class="toggle"><a href="#"><span class="bars"></span></a></li>
            </ul>
        </nav>
        <a class="contacter" href="logout.php"><button>Déconnexion</button></a>
        <?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <a class="contacter" href="logout.php"><button>Administration</button></a>
            <?php
        }
        ?>
</header>
<div class="bonjour">
    <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
</div>
<?php
$id=$_GET['id'];

$sql = "DELETE FROM `membres` WHERE `membres`.`id` = $id";
$req = $bdd->prepare($sql);
$req->execute();
$sql = "DELETE FROM `avis` WHERE `avis`.`id_membre` = $id";
$req = $bdd->prepare($sql);
$req->execute();
$sql = "DELETE FROM `preferences` WHERE `preferences`.`id_membre` = $id";
$req = $bdd->prepare($sql);
$req->execute();
header( "refresh:0;url=membres.php" );
?>
</body>
</html>