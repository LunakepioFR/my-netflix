<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function mysql_escape_mimic($inp) {
    if(is_array($inp))
        return array_map(__METHOD__, $inp);

    if(!empty($inp) && is_string($inp)) {
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
    }

    return $inp;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Administration MyNetflix</title>
</head>
<body>
<header>
        <img class="logo" src="logo.png" alt="logo du site"/>
		<form method="GET" action="recherche.php">
        <input class="recherche" name="recherche" type="text" placeholder="Rechercher..">
		</form>
        <nav>
            <ul class="lien_nav">
                <li class="items"><a href="index.php">Accueil</a></li>
                <li class="deroulant"><a href="#">Catégories</a></li>
                <li class="items"><a href="#">FAQ</a></li>
                <li class="toggle"><a href="#"><span class="bars"></span></a></li>
            </ul>
        </nav>
        <a class="contacter" href="logout.php"><button>Déconnexion</button></a>
        <?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <a class="contacter" href="administration.php"><button>Administration</button></a>
            <?php
        }
        ?>
</header>
<div class="bonjour">
    <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
</div>
<?php
$id=$_GET['id'];
$nom=$_POST['nom'];
$thematique=$_POST['thematique'];
$resume=$_POST['resume'];
$trailer=$_POST['trailer'];

$nom=mysql_escape_mimic($nom);
$resume=mysql_escape_mimic($resume);

$sql = "UPDATE `series` SET `nom` = '$nom', `thematique` = '$thematique', `resume` = '$resume', `trailer` = '$trailer'  WHERE `series`.`id` = $id";
$req = $bdd->prepare($sql);
$req->execute();

header( "refresh:0;url=updateseriee.php?id=$id" );
?>
</body>
</html>