<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

$admin = $_SESSION['admin'];

if ($admin == 0){
  header("location: identification.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$t = time();
?>
<html lang="en" dir="ltr" class="animate__animated animate__fadeIn">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="style.css?<?php echo $t ?>">
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <nav style="position:absolute; z-index:4; top:0%; width: 100%;">
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
<p class="bonjour" style="margin:25px;">Bonjour <?php 
echo $_SESSION['pseudo'];
?> <a href="preferences.php"><img width="16px" height="16px" src="settings.png" title="Paramètres"/></a></p>
<div class="area" >
            <ul class="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
            </ul>
    </div >


    <div class="liste">
    <div class="lien_nav">
        <div><a href="membres.php"><img src="lesmembres.png" >Pour accéder à l'espace <strong>membres</strong> cliquez <span class="liens">ICI</span></a></div>
        <div><a href="adfilm.php"><img src="seriesplay.png">Pour accéder à l'espace <strong>films/series</strong> cliquez <span class="liens">ICI</span></a></div>
        <div><a href="adavis.php"><img src="avis.png">Pour accéder à l'espace <strong>Avis</strong> cliquez <span class="liens">ICI</span></a></div>
        <div><a href="stats.php"><img src="stats.png">Pour accéder aux <strong>statistiques du site</strong> cliquez <span class="liens">ICI</span></a></div>
    </div>
    </div>

<script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>
</body>
</html>