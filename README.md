# Projet MyNetflix

Le projet MyNetflix est un projet en PHP HTML CSS où l'on doit réaliser un site de streaming à partir d'une base de données en MySQL

## Installation du projet (MAMP)

pour installer le projet il faut un serveur local avec lequel le code PHP va communiquer, pour se faire nous utilisons MAMP, qu'est une application qui permet de créer un serveur local facilement sans se soucier du backend,
vous pouvez suivre la procédure d'installation ici https://www.mamp.info/en/downloads/

## lancement du projet

pour lancer le projet, il faut que le point de serveur de mamp soit le fichier qui contient le code. 
il faut migrer la base de donnée (mynetflix.db) et enfin vous pouvez lancer le projet.

## login page

pour naviguer dans le site, vous devez d'abord être logué, pour cela vous pouvez créer un utilisateur dans la page indiqué ou vous logué en admin avec le log suivant :

userName : admin 
password : admin
