<?php 
session_start();
require('connexion.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="teest.css">
    <title>Document</title>
</head>
<body>
<div class="txt" contenteditable="true">
  MyNetflix
</div>
<iframe src="audio.mp3" allow="autoplay" style="display:none" id="iframeAudio">
</iframe> 
<audio autoplay loop  id="playAudio">
    <source src="audio.mp3">
</audio>
<script>
 var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
  if (!isChrome){
      $('#iframeAudio').remove()
  }
  else {
      $('#playAudio').remove() // just to make sure that it will not have 2x audio in the background 
  }
</script>
<?php
        header( "refresh:4;url=index.php");
?>
</body>
</html>
