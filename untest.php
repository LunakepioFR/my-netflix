<?php
session_start();

$t = time();
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" type="text/css" href="styleco.css?<?php echo $t ?>">
    <title>Page de connexion ou d'enregistrement</title>
</head>
<body>
<h1>Bienvenue dans </br><img class="logo" src="logo.png"/></h1>
    <div class="contenu">
        <div class="titre">
            <h2>Connectez-vous ou inscrivez-vous ici !</h2>
        </div>
        <div class="bloc">
            <div class="section1">
            <h3>Pour vous inscrire</h3>
                <form method="POST" action ="registre.php">
                    <input type="text" class="entree" name="pseudo" class="form-control" placeholder="Un pseudo" required>
                    <input type="text" class="entree" name="mail" class="form-control" placeholder="Adresse e-mail valide" required>
                    <input type="text" class="entree" name="secret" class="form-control" placeholder="Un indice pour votre mot de passe (Facultatif)">
                    <input type="password" class="entree" name="pass" class="form-control" placeholder="Mot de passe" required>
                    <button type="submit" class="btn">Inscription</button>
                </form>
            </div>
            <div class="section2">
            <h3>Pour vous connecter</h3>
            </br>
                    </br></br>
                <form method="POST" action ="loginverify.php">
                    <input type="text" class="entree" name="pseudo" class="form-control" placeholder="Votre pseudo" required>
                    <input type="password" class="entree" name="pass" class="form-control" placeholder="Mot de passe" required>
                    <?php 
                    $error=$_GET['error'];

                    if($error == "fail_pass")
                    {
                        ?>
                        <p class="test">ERREUR: </br>
                            Login ou mot de passe érroné
                        </p>
                        <?php
                    }
                    ?></br></br></br></br>
                <button type="submit" class="btn">Valider</button>
                </form>
            </div>
        </div>
    </div>

</body>
</html>