<?php 
session_start();
require('connexion.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pseudo = htmlspecialchars($_POST['pseudo'], ENT_COMPAT);
$pass = htmlspecialchars($_POST['pass'], ENT_COMPAT);
//  Récupération de l'utilisateur et de son pass hashé
$req = $bdd->prepare('SELECT * FROM membres WHERE pseudo = ?');
$req->execute(array($pseudo));

if ($req->rowCount() == 1) {
    $user = $req->fetch();
    if (password_verify($pass, $user['pass']))
     {
        $_SESSION['id'] = $user['id'];
        $_SESSION['pseudo'] = $user['pseudo'];
        $_SESSION['admin'] = $user['admin'];
        $_SESSION['auth'] = "OKAY";

        header( "refresh:0;url=intro.php");
    }
     else
         {
         echo "Boloss t'es pas co !";
         header( "refresh:0;url=identification.php?error=fail_pass" );
         echo 'You\'ll be redirected in about 5 secs. If not, click <a href="login.php">here</a>.';
         }
        }
        else
        {
            echo "CONNARD !";
            header( "refresh:0;url=identification.php?error=fail_pass" );
          echo 'You\'ll be redirected in about 5 secs. If not, click <a href="login.php">here</a>.';
        }
?>