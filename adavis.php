<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$admin = $_SESSION['admin'];

if ($admin == 0){
  header("location: identification.php");
}

$id=$_SESSION['id'];
$t = time();

?>
<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr" >
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="stylex.css?<?php echo $t ?>">
    <link rel="stylesheet" href="stylead.css?<?php echo $t ?>">
   <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


  </head>
  <body>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>

<div class="bonjour">
        <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </div>
        <?php
        $reponse = $bdd->query("SELECT DISTINCT avis.id_membre as idme, avis.id_serie as idse, avis.note, avis.commentaire as commentaire, avis.date_creation, series.id, series.nom as noms, membres.id, membres.pseudo as pseudo, preferences.id_membre, preferences.couleur_avis as couleur FROM avis, membres, series, preferences WHERE avis.id_serie = series.id AND membres.id = avis.id_membre AND preferences.id_membre = membres.id");

         ?>
         <div class="contenu-wrapper">
            <h2>Voici les différents avis publiés sur le site :</h2>
            <p>

            <table>
<thead>
        <tr>
            <th colspan="6">Avis</th>
        </tr>
    </thead>
    <tbody>
              <?php
            while ($donnees = $reponse->fetch())
            {
              $idme = $donnees['idme'];
              $idse = $donnees['idse'];
            ?>
            
                <tr>
            
                <td>Pseudo : <?php echo $donnees['pseudo'] ?></td>
                <td>Nom série : <?php echo $donnees['noms']?></td>
                <td>Commentaire : <?php echo $donnees['commentaire'] ?></td>
                <td><a href="deletecom.php?idme=<?php echo $idme ?>&idse=<?php echo $idse ?>"><span class="liens"><img width="50px"src="supprimer.png"></span></a></td>
                </tr>
            <?php
            }
            $reponse->closeCursor(); // Termine le traitement de la requête
            ?>
                
            </table>
            </div>
            <script>
            $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
      </script>
    </body>
    </html>