<?php
session_start();
require('connexion.php');

$t = time();

if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}


$admin = $_SESSION['admin'];

if ($admin == 0){
  header("location: identification.php");
}


function mysql_escape_mimic($inp) {
  if(is_array($inp))
      return array_map(__METHOD__, $inp);

  if(!empty($inp) && is_string($inp)) {
      return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
  }

  return $inp;
}

$reponse = $bdd->query('SELECT DISTINCT series.id as id, series.nom as nom, avis.id_serie, avis.note as note FROM series, avis WHERE avis.id_serie = series.id 
        ORDER BY `avis`.`note`  DESC LIMIT 10');

        $d = $_GET['istroisd'];

?>

<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->

    <link rel="stylesheet" href="style.css?<?php echo $t ?>">
    <link rel="stylesheet" href="<?php echo $theme ?>.css?<?php echo $t ?>">
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Film', 'Moyenne des notes'],
          <?php 
          while ($donnees = $reponse->fetch()){
          $nom = $donnees['nom'];
          $nom = mysql_escape_mimic($nom);
            echo "['".$nom."', ".$donnees["note"]."],";  
          } 
          ?> 
          
        ]);

        var options = {
            backgroundColor: 'transparent',
                   'width':1800,
                   'height':1200,
                   legend: {
        textStyle: { color: 'white' }

    },
          title: 'Top 10 des meilleurs series/film',
          is3D:false,
          <?php 
          if($d == true){
            echo "is3D:true";
          }
          ?>
        
        };

        

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

<style>
*{
  color: white;
}
</style>
  </head>
  <body>
    <nav style="position:absolute; top:0%; z-index:6; width:100%;">
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input class="supp" type="checkbox" id="btn">

      <ul>
        <li><form method="GET" action="recherche.php"><input class="recherche" name="recherche" type="text" placeholder="&#x1f50e; Rechercher.. "></form></li>
        <li><a href="index.php">Accueil</a></li>
        <li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input class="supp" type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">&Agrave; propos</a>
          <input class="supp" type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>

<div class="area" >
            <ul class="circles">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
            </ul>
    </div >



    <div id="piechart" style="width:2000px; height: 2000px;position:absolute; top:0%;z-index:5;"></div>
    <?php 
    if($d == true){
      echo '<a href="stats.php"><button style="position:absolute; top:50%;z-index:5">2D</button></a>';
    }
    else if($d == false)
    {
      echo '<a href="stats.php?istroisd=true"><button style="position:absolute; top:50%;z-index:5">3D</button></a>';
    }
    ?>



<script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>

  </body>
</html>
