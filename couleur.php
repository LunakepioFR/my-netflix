<?php
session_start();
require('connexion.php');

if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

$id = $_SESSION['id'];
$query = "SELECT couleur_avis FROM preferences WHERE id_membre =?";
$req = $bdd->prepare($query);
$req->execute(array($id));
$user_color = $req->fetch();

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />

        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Paramètres</title>
    </head>

    <body>
    <header>
        <img class="logo" src="logo.png" alt="logo du site"/>
        <form method="GET" action="recherche.php">
        <input class="recherche" name="recherche" type="text" placeholder="Rechercher..">
        </form>
        <nav>
            <ul class="lien_nav">
                <li class="items"><a href="index.php">Accueil</a></li>
                <li class="items"><a href="#">Catégories</a></li>
                <li class="items"><a href="#">FAQ</a></li>
                <li class="toggle"><a href="#"><span class="bars"></span></a></li>
            </ul>
        </nav>
        <a class="contacter" href="logout.php"><button>Déconnexion</button></a>
            
    </header>
    <div class="bonjour">
        <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </div>
    <div class="contenu">
        <form action="" method="post">
            <h2>Selectionnez la couleur qui encadrera vos avis :</h2>
            <p>
                <label for="couleur"><?php echo $_SESSION['pseudo']?></label> : <input type="color" name="couleur" id="couleur" value="<?php echo $user_color ? $user_color['couleur_avis'] : "#E50914"; ?>" /><br />    
                <button type="submit" class="valid">Valider</button>
            </p>
        </form>
    </div>

    <?php
        if (isset($_POST['couleur'])) {
            $couleur=$_POST['couleur'];
            $idm=$_SESSION['id'];

            if ($user_color)
                $sql = "UPDATE preferences SET couleur_avis = '{$couleur}' WHERE id_membre = '{$_SESSION['id']}'";
            else
                $sql ="INSERT INTO `preferences` (`id_membre`, `grille`, `couleur_avis`) VALUES ('$idm', '5', '$couleur');";
            $req = $bdd->prepare($sql);
            $req->execute();
        }
        header( "refresh:0;url=preferences.php");
    ?>

    </body>
</html>