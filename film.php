<?php
session_start();
require('connexion.php');

if(!isset($_SESSION['auth']))
{
    header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$id = 1;
$id = $_GET['id'];
$reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
$film = $reponse->fetch();

$notation = $bdd->query("SELECT AVG(note) as moyenne FROM avis WHERE id_serie='{$id}'");
$note = $notation->fetch();
$moyenne = $note['moyenne'];
$moyenne = round($moyenne);
$moyenne = $moyenne + 1;


?>
<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">

    <link rel="stylesheet" href="style.css">
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input type="checkbox" id="btn">
      <form method="GET" action="recherche.php">
      <input class="formu" type="text" name="recherche" placeholder="Rechercher..">
</form>
      <ul>
<li><a href="index.php">Accueil</a></li>
<li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">A propos</a>
          <input type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
  <section>

  </section>
    <div class="bonjour">
    <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </div>
    <div class="series">
        <div class="section">
            <h1>Nom : <?php echo $film['nom'] ?></h1>
        </div>
        <div class="section">
            <h2>Genre : <?php echo $film['thematique']?></h2>
        </div>
        <div class="section">
            <img width="314px" height="445px" src="<?php echo $film['image']?>"/>
        </div>
    
        <div class="section">
            <h2>Résumé :</h2>
            <p class="resumeee"><?php echo $film['resume'] ?></p>
        </div>
        <div class="section">
            <h2>Voir trailer :</h2>
            <iframe width="560" height="315" src="<?php echo $film['trailer']?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
        <div class="section">
        <h2>Note : <img src="<?php echo $moyenne?>.png" width="119px" /></h2> 
        </div>
        <section class="commentaires">
        <?php
            $reponse = $bdd->query("SELECT avis.id_membre, avis.commentaire as commentaire, avis.id_serie, avis.date_creation, membres.pseudo, preferences.id_membre, preferences.couleur_avis as couleur FROM avis, membres, preferences WHERE membres.id = avis.id_membre AND membres.id = preferences.id_membre AND avis.id_serie = '$id' ORDER BY date_creation ASC");
            // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
            ?>
                <div class="com" style="background-color:<?php echo $donnees['couleur']?>;">
                <h3><?php echo $donnees['pseudo'] ?> : </h3>
                <p><?php echo $donnees['commentaire'] ?></p>
                </div>
                </br>
            <?php
            }
            $reponse->closeCursor(); // Termine le traitement de la requête
            ?>
            <section>
            <form method="POST" action="com.php?id=<?php echo $id?>">
                <input class="formulaire" type="text" name="commentaire" placeholder="Ecrivez un commentaire .." required>
                <input class="formulaire" type="number" name="note" min="0" max="5" placeholder="/5" required/>
                <button class="contacter" type="submit">Envoyer</button>
            </form>
        </section>
        <script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>
</html>
