<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />

    <link rel="stylesheet" type="text/css" href="style.css">
    <title><?php
            $id = $_GET['id'];
            $reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
    
    // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
            ?>
            <?php echo $donnees['nom'] ?>
            <?php
            }
    
            $reponse->closeCursor(); // Termine le traitement de la requête
    
            ?></title>
</head>
<body>
<header>
        <img class="logo" src="logo.png" alt="logo du site"/>
        <form method="GET" action="recherche.php">
        <input class="recherche" name="recherche" type="text" placeholder="Rechercher..">
        </form>
        <nav>
            <ul class="lien_nav">
                <li class="items"><a href="index.php">Accueil</a></li>
                <li class="items"><a href="#">Catégories</a></li>
                <li class="items"><a href="#">FAQ</a></li>
                <li class="toggle"><a href="#"><span class="bars"></span></a></li>
            </ul>
        </nav>
        <a class="contacter" href="logout.php"><button>Déconnexion</button></a>
        
    </header>
    <div class="bonjour">
    <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </div>
    <div class="series">
            <div class="section">
            <?php
            $id = $_GET['id'];
            $idm = $_SESSION['idm'];
            $reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
    
    // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
            ?>
                <h1>Nom : <?php echo $donnees['nom'] ?></h1>
            <?php
            }
    
            $reponse->closeCursor(); // Termine le traitement de la requête
    
            ?>
            </div>
            <div class="section">
            <?php
            $reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
    
    // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
            ?>
                <h2>Genre : <?php echo $donnees['thematique']?></h2>
            <?php
            }
    
            $reponse->closeCursor(); // Termine le traitement de la requête
    
            ?>
            </div>
            <div class="section">
            <?php
            $reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
    
    // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
            ?>
                <img width="314px" height="445px" src="<?php echo $donnees['image']?>"/>
            <?php
            }
    
            $reponse->closeCursor(); // Termine le traitement de la requête
    
            ?>
            </div>
            <div class="section">
            <?php
            $reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
    
    // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
            ?>
                <h2>Résumé :</h2>
                <p class="resumeee"><?php echo $donnees['resume'] ?></p>
            <?php
            }
    
            $reponse->closeCursor(); // Termine le traitement de la requête
    
            ?>
            
            </div>
            <div class="section"><?php
            $reponse = $bdd->query("SELECT * FROM series WHERE id='{$id}'");
    
    // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
                $trailer=$donnees['trailer'];
            ?>
                
                <h2>Voir trailer :</h2>
                <iframe width="560" height="315" src="<?php echo $trailer?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            <?php
            }
    
            $reponse->closeCursor(); // Termine le traitement de la requête
    
            ?>
            </div>
            <section>
            <?php
            $reponse = $bdd->query("SELECT avis.id_membre, avis.commentaire as commentaire, avis.id_serie, avis.date_creation, membres.pseudo FROM avis, membres WHERE membres.id = avis.id_membre AND avis.id_serie = '$id' ORDER BY date_creation ASC");
    
    // On affiche chaque entrée une à une
            while ($donnees = $reponse->fetch())
            {
            ?>
                <h3><?php echo $donnees['pseudo'] ?> : </h3>
                <p><?php echo $donnees['commentaire'] ?></p>
            <?php
            }
    
            $reponse->closeCursor(); // Termine le traitement de la requête
    
            ?>
            </section>

            <?php echo '<div class="section">
            <form class="avis" method="POST" action="">
                <input type="text" name="commentaire" placeholder="Ecrivez un commentaire .." required>
                <input type="number" name="note" min="0" max="5" placeholder="/5" required/>
                <button class="contacter" type="submit">Envoyer</button>
            </form> '?>
            <?php 
            $idm = $_SESSION['id'];
            $ids = $id;
            $commentaire = $_POST['commentaire'];
            $note = $_POST['note'];
            
            $sql ="INSERT INTO `avis` (`id_membre`, `id_serie`, `note`, `date_creation`, `commentaire`) VALUES ('$idm', '$ids', '$note', CURRENT_TIMESTAMP, '$commentaire');";
            $req = $bdd->prepare($sql);
            $req->execute();
            ?>
        

            </div>
     </body>       
</html>