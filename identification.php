<?php
session_start();

$t = time();
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <link rel="stylesheet" type="text/css" href="retest.css?<?php echo $t ?>">
    <title>Page de connexion ou d'enregistrement</title>
</head>
<body>
    <style>
    #SLIDE_BG {
    width:100%;
    height: 100vh;
   background-position: center center;
   background-size:cover;
   background-repeat: no-repeat;
   backface-visibility: hidden;
   animation: slideBg 40s linear infinite alternate 0s;
   animation-timing-function: ease-in-out;
   background-image:  linear-gradient(to bottom, rgba(0,0,0, 1), rgba(0, 0, 0, 0) 10%), url('avengersbackground.jpg');
}

@keyframes slideBg{
    0% {
        background-image:url('avengersbackground.jpg'),url('hunterxhunterbackground.jpg');
    }
    12.5% {
        background-image:url('hunterxhunterbackground.jpg'),url('peakybackground.jpg');
    }
    25% { 
        background-image:url('peakybackground.jpg'),url('youbackground.jpg');
    }
    37.5% {
        background-image:url('youbackground.jpg'),url('starwarsbackground.jpg');
    }
    50% {
        background-image:url('starwarsbackground.jpg'),url('birdboxbackground.jpg');
    }
    62.5% {
        background-image:url('birdboxbackground.jpg'),url('horreur.jpg');
    }
    75% {
        background-image:url('horreur.jpg'),url('anime.jpg');
    }
    87.5% {
        background-image:url('anime.jpg'),url('outlanderbackground.jpg');
    }
    100% {
        background-image:url('outlanderbackground.jpg');
    }
}
.logo{
    font-family:'Poppins';
    font-size: 36px;
    font-weight: 600;
    text-align:left;
}

.banner{
    width:100%;
    height:100vh;
    overflow:hidden;
    display:flex;
    justify-content:center;
    align-items:center;
}

.banner video{
    position: absolute;
    top:0;
    left:0;
    object-fit:cover;
    width:100%;
    height:100%;
    pointer-events:none;
    z-index:-4;
}
.bienvenue{
    position:absolute;
    top:0%;
    left:0%;
}
    </style>
<!--<div class="hidden">
<img src="starwarsbackground.jpg">
    <img src="hunterxhunterbackground.jpg">
   <img src="avengersbackground.jpg">
   <img src="starwarsbackground.jpg">
   <img src="peakybackground.jpg">
   <img src="strangerthingsbackground.png">
   <img src="outlanderbackground.jpg">
   <img src="anime.jpg">
   <img src="birdboxbackground.jpg">
   <img src="starwarsbackground.jpg">
</div>
<div id="SLIDE_BG">-->

<div class="banner">
<video autoplay muted loop>
<source src="mynetflixtrailer.mp4" type="video/mp4">
</video>

</div>
<div class="bienvenue">
<h1>Bienvenue dans </br><span style="color:#E50914" class="logo">MyNetflix</h1></span>
</div>
    <div class="contenu">
        <div class="bloc">
            <div class="section2">
            <div class="titre">
            <h2>S'identifier</h2>
        </div>
                    <form method="POST" action ="loginverify.php">
                    <input type="text" class="entree" name="pseudo" class="form-control" placeholder="Votre pseudo" required>
                    <input type="password" class="entree" name="pass" class="form-control" placeholder="Mot de passe" required>
                    <?php 
                    $error=$_GET['error'];

                    if($error == "fail_pass")
                    {
                        ?>
                        <p class="test">ERREUR: </br>
                            Login ou mot de passe érroné
                        </p>
                        <?php
                    }
                    ?></br></br>
                    &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<button type="submit" class="btn">Valider</button>
                </form>
                </br>
                <p style="text-align:center;">Première visite sur MyNetflix ? <a href="inscription.php">Inscrivez-vous ici.</a></p>
            </div>
        </div>
    </div>
                </div>

</body>
</html>