<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>

<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="styles.css">
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input type="checkbox" id="btn">
      <ul>
<li><a href="index.php">Accueil</a></li>
<li>
          <label for="btn-1" class="show">Features +</label>
          <a href="#">Catégories</a>
          <input type="checkbox" id="btn-1">
          <ul>
<li><a href="action.php">Action</a></li>
<li><a href="horreur.php">Horreur</a></li>
<li><a href="anime.php">Anime</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">Services +</label>
          <a href="#">A propos</a>
          <input type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Mes paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
<div class="bonjour">
        <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </br>
    </br>
    <p>Dans la catégorie Super Héros :</p>
    </div>
    <div class="">
    <section>
    <div class="ligne">
        <div class="film">
        <?php
        $reponse = $bdd->query("SELECT * FROM `series` WHERE thematique = 'Super héros'");

// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
        ?>
            <a href="film.php?id=<?php echo $donnees['id']?>"><img width="204.1px" height="289.25px" src="<?php echo $donnees['image']?>" alt="Series"/></a>
        <?php
        }

        $reponse->closeCursor(); // Termine le traitement de la requête

        ?>
        </div>
    </div>
    
    </section>
</div>
<script>
      $('.icon').click(function(){
        $('span').toggleClass("cancel");
      });
    </script>

  </body>
</html>
