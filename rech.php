<?php
session_start();
require('connexion.php');


if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

?>

<!DOCTYPE html>
<!-- Created By CodingNepal -->
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- Somehow I got an error, so I comment the title, just uncomment to show -->
    <!-- <title>Responsive Drop-down Menu Bar</title> -->
    <link rel="stylesheet" href="style.css">
    
    <script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <nav>
      <div style="color:#E50914" class="logo">
MyNetflix</div>
<label for="btn" class="icon">
        <span class="fa fa-bars"></span>
      </label>
      <input type="checkbox" id="btn">
      <form method="GET" action="recherche.php">
      <input class="formu" type="text" name="recherche" placeholder="Rechercher..">
</form>
      <ul>
        <li><a href="rech.php">Rechercher</a></li></li>
<li><a href="index.php">Accueil</a></li>
<li>
          <label for="btn-1" class="show">Catégories +</label>
          <a href="#">Catégories</a>
          <input type="checkbox" id="btn-1">
          <ul>
<li><a href="categorie.php?theme=Horreur">Horreur</a></li>
<li><a href="categorie.php?theme=anime">Anime</a></li>
<li><a href="categorie.php?theme=Super">Super Heros</a></li>
<li><a href="categorie.php?theme=thriller">Thriller</a></li>
</ul>
</li>
<li>
          <label for="btn-2" class="show">A propos +</label>
          <a href="#">A propos</a>
          <input type="checkbox" id="btn-2">
          <ul>
<li><a href="myavis.php">Mes Avis</a></li>
<li><a href="preferences.php">Paramètres</a></li>
</ul>
</li>
<li><a href="logout.php">Déconnexion</a></li>
<?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <li><a href="administration.php">Administration</a></li>
            <?php
        }
        ?>
</ul>
</nav>
<div class="bonjour">
        <p>Bonjour <?php echo $_SESSION['pseudo']?> </p>
    </div>

<div class="contenu">
        <h2>Rechercher sur ce site :</h2>
        <form method="GET" action="recherche.php">
            <input type="text" placeholder="Rechercher..">
        </form>
</div>