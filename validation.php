<?php 
session_start();
require('connexion.php');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pseudo = htmlspecialchars($_POST['pseudo'], ENT_COMPAT);
$pass = htmlspecialchars($_POST['pass'], ENT_COMPAT);
//  Récupération de l'utilisateur et de son pass hashé
$req = $bdd->prepare('SELECT * FROM membres WHERE pseudo = ?');
$req->execute(array($pseudo));

if ($req->rowCount() == 1) {
    $user = $req->fetch();
    if (password_verify($pass, $user['pass'])) {
        $_SESSION['id'] = $user['id'];
        $_SESSION['pseudo'] = $user['pseudo'];
        $_SESSION['admin'] = $user['admin'];
        $_SESSION['auth'] = "OKAY";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Accueil MyNetflix</title>
</head>
<body>
<header>
        <img class="logo" src="logo.png" alt="logo du site"/>
        <input class="recherche" type="text" placeholder="Rechercher..">
        <nav>
            <ul class="lien_nav">
                <li class="items"><a href="validation.php">Accueil</a></li>
                <li class="items"><a href="#">Catégories</a></li>
                <li class="items"><a href="#">FAQ</a></li>
                <li class="toggle"><a href="#"><span class="bars"></span></a></li>
            </ul>
        </nav>
        <a class="contacter" href="logout.php"><button>Déconnexion</button></a>
        <?php 
        if($_SESSION['admin']== 1)
        {
            ?>
            <a class="contacter" href="administration.php"><button>Administration</button></a>
            <?php
        }
        ?>
    </header>
    <p>Bonjour <?php 
    echo $_SESSION['pseudo'];
    ?> </p>
    <section>
    <div class="ligne">
        <div class="film">
        <?php
        $reponse = $bdd->query('SELECT * FROM series');

// On affiche chaque entrée une à une
        while ($donnees = $reponse->fetch())
        {
        ?>
            <a href="film.php?id=<?php echo $donnees['id']?>"><img width="204.1px" height="289.25px" src="<?php echo $donnees['image']?>" alt="Series"/></a>
        <?php
        }

        $reponse->closeCursor(); // Termine le traitement de la requête

        ?>
        </div>
    </div>
    
    </section>
</body>
<?php
if(!isset($_SESSION['auth']))
{
header("Location: untest.php");
}
?>
</html>
<?php
// note : stocké l'url de l'image à mettre dans la bdd
    }
    else {
        echo "Boloss t'es pas co !";
        header( "refresh:0;url=untest.php?error=fail_pass" );
  echo 'You\'ll be redirected in about 5 secs. If not, click <a href="login.php">here</a>.';
    }
}
else {
    echo "CONNARD !";
    header( "refresh:0;url=untest.php?error=fail_pass" );
  echo 'You\'ll be redirected in about 5 secs. If not, click <a href="login.php">here</a>.';
}
?>
